let express = require('express');
let bodyParser = require('body-parser');
app = express();
let morgan = require('morgan');
let env = require('node-env-file');
let bcrypt = require('bcrypt');
let session = require('express-session');
admin = require('firebase-admin');


var serviceAccount = require("./app/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://api-hajj.firebaseio.com"
});

const MongoStore = require('connect-mongo')(session);

_ = require('lodash');
path = require('path');
Sequence = exports.Sequence || require('sequence').Sequence, sequence = Sequence.create();

env(__dirname + '/.env');

let helmet = require('helmet');
app.use(helmet());
app.disable('x-powered-by');
app.use(helmet.noSniff())
require(process.env.SECURITY_CONFIG);

app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
        url: process.env.MONGO_URL,
        autoRemove: 'disabled'
    })
}))

app.use(morgan('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json());

let port = process.env.PORT || 3000;
let mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL,{ useNewUrlParser: true });

let router = require("./app/routes/index");
router.use(function(req, res, next) {
    next();
});

app.use('/v1/', router);
router.get('/', function(req, res) {
    res.json({ message: 'version 1.0' });
});
app.listen(port);
console.log('Webservice running on ' + port);
