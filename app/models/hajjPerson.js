let mongoose = require('mongoose');

let HajjPersonSchema = new mongoose.Schema({
    code: {
        type: String,
        unique: true
    },
    fullname: String,
    from: String,
    phone: String,
    hotel: String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    deleted_at: { type: Date, default: null }
}, { strict: true });

module.exports = mongoose.model('HajjPerson', HajjPersonSchema);