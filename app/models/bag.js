let mongoose = require('mongoose');

let BagSchema = new mongoose.Schema({
    barcode: {
        type: String,
        unique: true
    },
    hajj_person: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'hajjPerson'
    },
    from: String,
    to: String,
    departure_date: { type: Date, default: null },
    date_of_coming: { type: Date, default: null },
    status: { type: String, default: 'initial' },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    deleted_at: { type: Date, default: null }
}, { strict: true });
module.exports = mongoose.model('Bag', BagSchema);
