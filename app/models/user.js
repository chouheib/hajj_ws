let mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
    barcode: {
        type: String,
        unique: true
    },
    timezone: String,
    locale: String,
    fullname: String,
    phone: String,
    adresse: String,
    photo: String,
    isactif: Number,
    email: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    deleted_at: { type: Date, default: null }
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
}, { strict: true });

UserSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.password;
        return ret;
    }
});
module.exports = mongoose.model('User', UserSchema);
