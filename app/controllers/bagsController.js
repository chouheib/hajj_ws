"use strict";
let Bag   = require('../models/bag');
let HajjPerson   = require('../models/hajjPerson');
let router = require('express').Router();

let sendNotification = function(bag_id, status){
    console.log('sendNotification' );
}

router.route('/')
    .post(function(req, res) {
        let bag                 = new Bag();
        bag.barcode             = req.body.barcode;

        HajjPerson.findOne({code:req.body.qrcode}).exec((err, hajj_person) =>{
            if (err) res.send(err);
            if(hajj_person){
                bag.hajj_person         = hajj_person._id;
                bag.from                = req.body.from;
                bag.to                  = req.body.to;
                bag.status              = req.body.status;         
                bag.date_of_coming      = req.body.date_of_coming;         
                bag.departure_date      = req.body.departure_date;         
                bag.save(function(err, data) {
                    if (err) res.send(err);
                    res.json(data);
                });
            }else{
                res.json({ error:true, message:'Hajj not found'});
            }
        });
    })
    .get(function(req, res) {
        Bag.find({}).exec((err, data) =>{
            if (err) res.send(err);
            res.json(data);
        });
    });

router.route('/:bar_code')
    .put(function(req, res) {
        Bag.findOne({barcode:req.params.bar_code}, function(err, bag) {
            if (err) res.send(err);
            req.body.barcode             ? bag.barcode             = req.params.bar_code         :null;
            req.body.hajj_person         ? bag.hajj_person         = req.body.hajj_person        :null;
            req.body.from                ? bag.from                = req.body.from               :null;
            req.body.to                  ? bag.to                  = req.body.to                 :null;
            req.body.status              ? bag.status              = req.body.status             :null;
            req.body.date_of_coming      ? bag.date_of_coming      = req.body.date_of_coming     :null;
            req.body.departure_date      ? bag.departure_date      = req.body.departure_date     :null;

            bag.save(function(err, bag) {
                if (err) res.send(err);
                let status;
                switch(bag.status) {
                    case 'sended':
                        status = 'sended';
                        break;
                    case 'delivred':
                        status = 'delivred';
                        break;
                    default:;
                }
                sendNotification(req.params.id,status);
                res.json(bag);
            });
        });
    });


router.route('/:id')
    .delete(function(req, res) {
        Bag.remove({ _id: req.params.id }, function(err, bag) {
            if (err) res.send(err);
            res.json({ success: 'true' });
        });
    });
module.exports = router;