"use strict";
let HajjPerson   = require('../models/hajjPerson');
let router = require('express').Router();

router.route('/')
    .post(function(req, res) {
        let hajjPerson         = new HajjPerson();
        hajjPerson.code        = req.body.code;
		hajjPerson.fullname    = req.body.fullname;
		hajjPerson.phone       = req.body.phone;
		hajjPerson.from        = req.body.from;
		hajjPerson.hotel       = req.body.hotel;   
        hajjPerson.save(function(err, data) {
            if (err) res.send(err);
            res.json(data);
        });
    })
    .get(function(req, res) {
        HajjPerson.find({}).exec((err, data) =>{
            if (err) res.send(err);
            res.json(data);
        });
    });

router.route('/:id')
    .delete(function(req, res) {
        HajjPerson.remove({ _id: req.params.id }, function(err, user) {
            if (err) res.send(err);
            res.json({ success: 'true' });
        });
    });

module.exports = router;
