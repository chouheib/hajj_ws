"use strict";
let User   = require('../models/user');
let router = require('express').Router();
let bcrypt = require('bcrypt');
let fs     = require('fs');

router.route('/')
    .post(function(req, res) {
        let user      = new User();
        user.barcode  = req.body.barcode;
        user.timezone = req.body.timezone;
        user.locale   = req.body.locale;
        user.fullname = req.body.fullname;
        user.phone    = req.body.phone;
        user.adresse  = req.body.adresse;
        user.isactif  = req.body.isactif;
        user.email    = req.body.email;
        
        if (req.body.password) {
            user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync());
        }
        user.save(function(err, data) {
            if (err) res.send(err);
            res.json(data);
        });
    })
    .get(function(req, res) {
        User.find({}).exec((err, data) =>{
            if (err) res.send(err);
            res.json(data);
        });
    });
router.route('/login')
    .post(function(req, res) {
        let query = {};
        if(req.body.email){
            query.email= req.body.email;
        }
        if(req.body.barcode){
            query.barcode= req.body.barcode;
        }
        if((!query.email && !query.barcode)){
            res.send(403,{ 'status':false });
        }
        User.findOne(query)
            .populate('role')
            .exec(function(err, user) {
            if (err) res.send(err);
            if (user && user.password) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    if( user.isactif == 0 ){
                        res.send(403, { 'status':false, 'error': 'Inactif' });
                    }else{
                        req.session.me = user._id;
                        res.send({'status':true, 'user':user});
                    }
                } else {
                    res.send(403, { 'status':false, 'error': 'Mot de passe incorrecte' });
                }
            }
            res.send(403,{ 'status':false, 'error': (req.body.email?'email':'code a barre')+' incorrecte' });
        });
    });
router.route('/authenticated')
    .get(function(req, res) {
        User.findById(req.session.me).populate('role').exec((err, user) => {
            if (err) res.send(err);
            res.send(user);
        });
    });
router.route('/logout')
    .get(function(req, res) {
        req.session.destroy();
        res.send('success');
    });
router.route('/:id/photo') 
    .put(function(req, res) {
        User.findById(req.params.id, function(err, user) {
            if (err) res.send(err);
            let base64Data = req.body.photo.replace(/^data:image\/png;base64,/, "").replace(/^data:image\/jpeg;base64,/, "");
            let photo = "/uploads/photos/users/" + req.params.id + ".png";
            fs.writeFile(process.env.PROJECT_PATH+"/public" + photo, base64Data, 'base64', ()=>{});
            let path = process.env.DOMAINE_NAME + photo;
            User.update({ '_id': req.params.id }, { photo: path }, { multi: true }, function(err) {
                res.json({success:true});
            });
        });
    });
router.route('/:id')
    .get(function(req, res) {
        User.findOne({_id: req.params.id}).exec((err, data) =>{
            if (err) res.send(err);
            res.json(data);
        });
    })
    .put(function(req, res) {
        User.findById(req.params.id, function(err, user) {
            if (err) res.send(err);
            
            req.body.barcode ? user.barcode = req.body.barcode:null;
            req.body.timezone ? user.timezone = req.body.timezone:null;
            req.body.locale ? user.locale = req.body.locale:null;
            req.body.fullname ? user.fullname = req.body.fullname:null;
            req.body.phone ? user.phone = req.body.phone:null;
            req.body.adresse ? user.adresse = req.body.adresse:null;
            req.body.isactif ? user.isactif = req.body.isactif:null;
            req.body.email ? user.email = req.body.email:null;
            req.body.role ? user.role = req.body.role:null;
            req.body.societe ? user.societe = req.body.societe:null;
            if (req.body.password) {
                user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync());
            }
            user.save(function(err, data) {
                if (err) res.send(err);
                res.json(data);
            });
        });
    })
    .delete(function(req, res) {
        User.remove({ _id: req.params.id }, function(err, user) {
            if (err) res.send(err);
            res.json({ success: 'true' });
        });
    });


module.exports = router;
