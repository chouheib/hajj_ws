let express = require('express');
let router = express.Router();
let fs = require('fs');


let routes = fs.readdirSync(__dirname + "/../controllers").filter(function(v) {
    return (/^(?!index\.js)..*\.js$/).test(v);
});
routes.forEach(function(file) {
    let name = file.substr(0, file.indexOf('.'));
    let route_name = name.substr(0, name.indexOf("Controller"));
    router.use('/' + route_name, require('../controllers/' + name));
});
module.exports = router;
