app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Origin', 'https://creative.tn');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);

    if( req.headers.origin != 'https://creative.tn' ){
      res.send(403, 'not authorized');
    }else{
      next();
    }
});