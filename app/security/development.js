app.use(function(req, res, next) {
    let allowedOrigins = [];
    let origin = req.headers.origin;
    if(origin){
        allowedOrigins.push(origin);
    }
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    res.setHeader("Expires", new Date(Date.now() + 60 * 60 * 24 * 365).toUTCString());
    next();
});
